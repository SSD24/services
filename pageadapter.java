package com.myapp.myapplication;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class pageadapter extends FragmentPagerAdapter {

    int tabcounter;
    public pageadapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        tabcounter=behavior;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch(position)
        {
            case 0: return new BlankFragment();
            case 1: return new BlankFragment2();
            default:return null;
        }
    }

    @Override
    public int getCount() {
        return 0;
    }
}
